from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

from .form import StreamerForm
from .models import Streamer

from .utils import handle_uploaded_file, set_id, get_random_instance


def home(request):
    streamer_form = StreamerForm()
    streamer = get_random_instance(Streamer)

    if request.method == 'POST':
        streamer_form = StreamerForm(request.POST, request.FILES)
        if streamer_form.is_valid():
            new_streamer = Streamer()
            new_streamer.id = set_id(Streamer)
            new_streamer.nom = streamer_form.cleaned_data['nom']
            new_streamer.description = streamer_form.cleaned_data['description']
            new_streamer.image = handle_uploaded_file(request.FILES["image"])
            new_streamer.save()

    return render(request, 'streamer_site/home.html', {
        'nom': streamer.nom,
        'description': streamer.description,
        'image': streamer.image,
        'streamer_form': streamer_form
    })


def showStreamer(request, pk):
    try:
        streamer = Streamer.objects.get(pk=pk)
        return render(request, 'streamer_site/streamer.html', {
            'nom': streamer.nom,
            'description': streamer.description,
            'image': streamer.image
        })
    except ObjectDoesNotExist:
        return render(request, 'streamer_site/notFound.html')





def chatbot(request):
    return render(request, 'streamer_site/chatbot.html')
