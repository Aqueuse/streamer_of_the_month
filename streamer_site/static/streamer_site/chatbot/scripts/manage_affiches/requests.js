function create(json) {
    const url = 'https://127.0.0.1:8000/affiches/';
    let request = new Request(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest', //Necessary to work with request.is_ajax()
        },
        body: JSON.stringify(json)
    });

    fetch(request)
        .then(response => {
            return response.status;
        });
}

function retrieve(name) {
    const url = 'http://127.0.0.1:8000/affiches/?nom='+name;
    let request = new Request(url, {
        method: 'GET',
        headers: {
            'X-Requested-With': 'XMLHttpRequest', //Necessary to work with request.is_ajax()
        }
    });

    fetch(request)
        .then(response => {
            return response.json();
        });
}

function update(name, json) {
    const url = 'https://127.0.0.1:8000/affiches/';
    let request = new Request(url, {
        method: 'UPDATE',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest', //Necessary to work with request.is_ajax()
        },
        body: {'name': name, 'json': json}
    });

    fetch(request)
        .then(response => {
            return response.status;
        });
}

function do_delete(name) {
    const url = 'https://127.0.0.1:8000/affiches/';
    let request = new Request(url, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: {'name': name}
    });

    fetch(request)
        .then(response => {
            return response.status;
        });
}
