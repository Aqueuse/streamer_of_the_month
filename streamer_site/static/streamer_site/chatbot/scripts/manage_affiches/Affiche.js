/**
 * Represents an affiche
 * @constructor
 * @param {string} nom
 * @param {string} description
 * @param {string} image
 *
 */
function Affiche(nom, description, image) {
    const affiche = {
        nom: '',
        description: '',
        image: ''
    }

    let new_affiche = Object.create(affiche);

    new_affiche.nom = nom;
    new_affiche.description = description;
    new_affiche.image = image;

    return new_affiche;
}