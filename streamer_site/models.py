from django.db import models


class Streamer(models.Model):
    id = models.PositiveIntegerField(unique=True, primary_key=True)
    nom = models.CharField(blank=True, max_length=100)
    description = models.TextField(blank=True)
    image = models.FileField(upload_to='streamer_site/static/images', null=True)

    def __str__(self):
        return f"{self.nom}"
