import random

from django.db.models import Max


def get_random_instance(model):
    streamer_site = list(model.objects.all())
    return streamer_site[random.randint(0, len(streamer_site) - 1)]


def handle_uploaded_file(f):
    with open('streamer_site/static/images/' + f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return f.name


def set_id(model):
    max_id = model.objects.all().aggregate(Max('id'))
    return max_id.get('id__max') + 1
