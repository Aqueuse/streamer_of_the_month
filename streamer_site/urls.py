from django.urls import path

from .views import home, chatbot, showStreamer
from .restAPI import AfficheList, AfficheView

urlpatterns = [
    path('', home),
    path('streamer/<int:pk>/', showStreamer),
    path('chat/', chatbot),
    path('affiches/', AfficheList.as_view()),
    path('affiches/<int:pk>/', AfficheView.as_view())
]
