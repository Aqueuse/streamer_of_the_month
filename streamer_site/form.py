from django import forms


class StreamerForm(forms.Form):
    nom = forms.CharField(
        max_length=100,
        required=True,
        label=False,
        widget=forms.TextInput(attrs={'placeholder': 'Nom'})
    )
    description = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': 'Description'}),
        required=True,
        label=False
    )
    image = forms.FileField(required=True)
