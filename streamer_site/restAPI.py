from django.http import JsonResponse, Http404

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .serializers import StreamerSerializer

from .models import Streamer
from .utils import handle_uploaded_file, set_id


class AfficheList(APIView):
    def get(self, request, format=None):
        streamers = Streamer.objects.all()
        serializer = StreamerSerializer(streamers, many=True)
        return JsonResponse({'code': 201})

    def post(self, request, format=None):
        serializer = StreamerSerializer(data=request.data)

        if serializer.is_valid():
            serializer.validated_data['id'] = set_id(Streamer)
            serializer.validated_data['image'] = handle_uploaded_file(request.FILES["image"])
            serializer.save()
            return JsonResponse({'code': 201})
        return JsonResponse({'code': 404})


class AfficheView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return Streamer.objects.get(pk=pk)
        except Streamer.DoesNotExist:
            return JsonResponse({'code': 404})

    def get(self, request, pk, format=None):
        streamer = self.get_object(pk)
        serializer = StreamerSerializer(streamer)
        return JsonResponse({'nom': serializer.data['nom'], 'description': serializer.data['description']})

    def post(self, request, format=None):
        serializer = StreamerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({'code': 201})
        return JsonResponse({'code': 404})

    def put(self, request, pk, format=None):
        streamer = self.get_object(pk)
        serializer = StreamerSerializer(streamer, data=request.data)
        if serializer.is_valid():
            serializer.validated_data['image'] = handle_uploaded_file(request.FILES["image"])
            serializer.save()
            return JsonResponse({'code':201})
        return JsonResponse({'code': 404})

    def delete(self, request, pk, format=None):
        streamer = self.get_object(pk)
        print(streamer.nom)
        streamer.delete()
        return JsonResponse({'code': 201})
