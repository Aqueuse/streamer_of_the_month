# Generated by Django 3.2.4 on 2021-06-29 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Streamer',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False, unique=True)),
                ('description', models.TextField(blank=True)),
                ('img', models.FileField(upload_to='streamer_site/static/images')),
            ],
        ),
    ]
